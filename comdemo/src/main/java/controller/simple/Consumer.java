package controller.simple;

import org.apache.activemq.command.Message;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @program: Consumer
 * @author: 张磊
 * @create: 2019/5/27-15:35
 **/
@Component
public class Consumer {

    @JmsListener(destination="routKey")
    public void readMessage(String text, Message message){
        System.out.println("收到消息："+text);
    }
}
